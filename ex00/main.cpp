#include "Cat.hpp"
#include "Dog.hpp"
#include "WrongAnimal.hpp"
#include "WrongCat.hpp"
#include <iostream>

int main()
{
	{
		std::cout << "\npdf test:\n" << std::endl;
		const Animal*	meta = new Animal();
		const Animal*	j = new Dog();
		const Animal*	i = new Cat();

		std::cout << j->getType() << " " << std::endl;
		std::cout << i->getType() << " " << std::endl;
		i->makeSound(); //will output the cat sound!
		j->makeSound();
		meta->makeSound();
	}
	{
		std::cout << "\npdf test 2:\n" << std::endl;
		const WrongAnimal*	meta = new WrongAnimal();
		const WrongAnimal*	i = new WrongCat();

		std::cout << i->getType() << " " << std::endl;
		i->makeSound(); //will output the cat sound!
		meta->makeSound();
	}
	{
		std::cout << "\ndog copy:\n" << std::endl;
		Dog				*dog1 = new Dog;
		Dog				*dog2 = new Dog(*dog1);

		delete dog1;
		delete dog2;
	}
	{
		std::cout << "\ndog copy (overload):\n" << std::endl;
		Dog				*dog1 = new Dog;
		Dog				*dog2 = new Dog;

		*dog1 = *dog2;
		delete dog1;
		delete dog2;
	}
	{
		std::cout << "\ncat copy:\n" << std::endl;
		Cat				*cat1 = new Cat;
		Cat				*cat2 = new Cat(*cat1);

		delete cat1;
		delete cat2;
	}
	{
		std::cout << "\ncat copy (overload):\n" << std::endl;
		Cat				*cat1 = new Cat;
		Cat				*cat2 = new Cat;

		*cat1 = *cat2;
		delete cat1;
		delete cat2;
	}
	return (0);
}
