#include "Cat.hpp"
#include <iostream>

Cat::~Cat(void)
{
	std::cout << "Cat destroyed." << std::endl;
}

Cat::Cat(void)
{
	type = "Cat";
	std::cout << "Cat created." << std::endl;
}

Cat::Cat(const Cat &copy)
{
	*this = copy;
}

void	Cat::makeSound(void) const
{
	std::cout << "Meow." << std::endl;
}

Cat		&Cat::operator =(const Cat &copy)
{
	type = copy.type;
	std::cout << "Cat created from copy." << std::endl;

	return (*this);
}
