#include "Dog.hpp"
#include <iostream>

Dog::~Dog(void)
{
	std::cout << "Dog destroyed." << std::endl;
}

Dog::Dog(void)
{
	type = "Dog";
	std::cout << "Dog created." << std::endl;
}

Dog::Dog(const Dog &copy)
{
	*this = copy;
}

void	Dog::makeSound(void) const
{
	std::cout << "Woof." << std::endl;
}

Dog		&Dog::operator =(const Dog &copy)
{
	type = copy.type;
	std::cout << "Dog created from copy." << std::endl;

	return (*this);
}
