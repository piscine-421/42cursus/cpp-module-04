#ifndef ANIMAL_HPP
# define ANIMAL_HPP
# include <string>

class	Animal
{
	public:
		virtual			~Animal(void);
		Animal(void);
		Animal(const Animal &copy);
		std::string		getType(void) const;
		virtual void	makeSound(void) const;
		Animal			&operator =(const Animal &copy);
	protected:
		std::string		type;
};

#endif
