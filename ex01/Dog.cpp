#include "Dog.hpp"
#include <iostream>

Dog::~Dog(void)
{
	delete brain;
	std::cout << "Dog destroyed." << std::endl;
}

Dog::Dog(void)
{
	type = "Dog";
	brain = new Brain();
	std::cout << "Dog created." << std::endl;
}

Dog::Dog(const Dog &copy): Animal(copy)
{
	brain = new Brain(*copy.brain);
	*this = copy;
}

Brain	&Dog::get_brain(void) const
{
	return (*brain);
}

void	Dog::makeSound(void) const
{
	std::cout << "Woof." << std::endl;
}

Dog		&Dog::operator =(const Dog &copy)
{
	Animal::operator=(copy);
	delete brain;
	brain = new Brain(*copy.brain);
	std::cout << "Dog created from copy." << std::endl;

	return (*this);
}
