/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aperez-b <aperez-b@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/30 13:07:11 by aperez-b          #+#    #+#             */
/*   Updated: 2022/09/30 13:07:13 by aperez-b         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Animal.hpp"
#include "Dog.hpp"
#include "Cat.hpp"
#include <iostream>

int main(void)
{
	{
		std::cout << "\npdf test 1:\n" << std::endl;
		const Animal	*j = new Dog();
		const Animal	*i = new Cat();
		delete j;//should not create a leak
		delete i;
	}
	{
		std::cout << "\npdf test 2:\n" << std::endl;
		const Animal	*animals[10];

		for (int i = 0; i < 10; i++)
		{
			if (i < 10 / 2)
				animals[i] = new Dog();
			else
				animals[i] = new Cat();
		}
		std::cout << std::endl;
		std::cout << animals[0]->getType() << std::endl;
		std::cout << animals[5]->getType() << std::endl;
		Brain			*brain;
		brain = &animals[0]->get_brain();
		brain->set_idea(0, "no clue");
		brain->set_idea(1, "am animal");
		brain->set_idea(2, "head empty");
		std::cout << animals[0]->get_brain().get_idea(0) << std::endl;
		std::cout << animals[0]->get_brain().get_idea(1) << std::endl;
		std::cout << std::endl;
		for (int i = 0; i < 10; i++)
			delete animals[i];
	}
	{
		std::cout << "\ndog copy:\n" << std::endl;
		Dog				*dog1 = new Dog;
		Dog				*dog2 = new Dog(*dog1);

		delete dog1;
		delete dog2;
	}
	{
		std::cout << "\ndog copy (overload):\n" << std::endl;
		Dog				*dog1 = new Dog;
		Dog				*dog2 = new Dog;

		*dog1 = *dog2;
		delete dog1;
		delete dog2;
	}
	{
		std::cout << "\ncat copy:\n" << std::endl;
		Cat				*cat1 = new Cat;
		Cat				*cat2 = new Cat(*cat1);

		delete cat1;
		delete cat2;
	}
	{
		std::cout << "\ncat copy (overload):\n" << std::endl;
		Cat				*cat1 = new Cat;
		Cat				*cat2 = new Cat;

		*cat1 = *cat2;
		delete cat1;
		delete cat2;
	}
	return (0);
}
