#ifndef ANIMAL_HPP
# define ANIMAL_HPP
# include "Brain.hpp"
# include <string>

class	Animal
{
	public:
		virtual			~Animal(void);
		Animal(void);
		Animal(const Animal &copy);
		virtual Brain	&get_brain(void) const = 0;
		std::string		getType(void) const;
		virtual void	makeSound(void) const;
		Animal			&operator =(const Animal &copy);
	protected:
		std::string	type;
};

#endif
