#include "Cat.hpp"
#include <iostream>

Cat::~Cat(void)
{
	delete brain;
	std::cout << "Cat destroyed." << std::endl;
}

Cat::Cat(void)
{
	type = "Cat";
	brain = new Brain();
	std::cout << "Cat created." << std::endl;
}

Cat::Cat(const Cat &copy): Animal(copy)
{
	brain = new Brain(*copy.brain);
	*this = copy;
}

Brain	&Cat::get_brain(void) const
{
	return (*brain);
}

void	Cat::makeSound(void) const
{
	std::cout << "Meow." << std::endl;
}

Cat		&Cat::operator =(const Cat &copy)
{
	Animal::operator=(copy);
	delete brain;
	brain = new Brain(*copy.brain);
	std::cout << "Cat created from copy." << std::endl;

	return (*this);
}
