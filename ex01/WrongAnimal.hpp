#ifndef WRONGANIMAL_HPP
# define WRONGANIMAL_HPP
# include <string>

class	WrongAnimal
{
	public:
		~WrongAnimal(void);
		WrongAnimal(void);
		WrongAnimal(const WrongAnimal &copy);
		std::string	getType(void) const;
		void		makeSound(void) const;
		WrongAnimal	&operator=(const WrongAnimal &copy);
	protected:
		std::string	type;
};

#endif
