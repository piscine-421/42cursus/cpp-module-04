#ifndef CAT_HPP
# define CAT_HPP
# include "Animal.hpp"
# include "Brain.hpp"

class Cat: public Animal
{
	public:
		~Cat(void);
		Cat(void);
		Cat(const Cat &copy);
		Brain		&get_brain(void) const;
		void		makeSound(void) const;
		Cat			&operator =(const Cat &copy);
	private:
		Brain	*brain;
};

#endif
