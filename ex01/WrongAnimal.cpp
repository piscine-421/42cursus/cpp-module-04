#include "WrongAnimal.hpp"
#include <iostream>

WrongAnimal::~WrongAnimal(void)
{
	std::cout << "WrongAnimal of type " << type << " destroyed." << std::endl;
}

WrongAnimal::WrongAnimal(void)
{
	type = "none";
	std::cout << "WrongAnimal created." << std::endl;
}

WrongAnimal::WrongAnimal(const WrongAnimal &copy)
{
	*this = copy;
}

std::string	WrongAnimal::getType(void) const
{
	return (type);
}

void		WrongAnimal::makeSound(void) const
{
	std::cout << "WrongAnimal sound." << std::endl;
}

WrongAnimal	&WrongAnimal::operator =(const WrongAnimal &copy)
{
	type = copy.getType();
	std::cout << "WrongAnimal of type " << type << " created from copy." << std::endl;

	return (*this);
}
