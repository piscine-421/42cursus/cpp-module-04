#ifndef BRAIN_HPP
# define BRAIN_HPP
# include <string>

class Brain
{
	public:
		~Brain(void);
		Brain(void);
		Brain(const Brain &copy);
		std::string	get_idea(int i);
		Brain		&operator =(const Brain &copy);
		void		set_idea(int i, std::string value);
	private:
		std::string	ideas[100];
};

#endif
