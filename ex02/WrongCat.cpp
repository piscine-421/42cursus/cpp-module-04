#include "WrongCat.hpp"
#include <iostream>

WrongCat::~WrongCat(void)
{
	std::cout << "WrongCat destroyed." << std::endl;
}

WrongCat::WrongCat(void)
{
	type = "WrongCat";
	std::cout << "WrongCat created." << std::endl;
}

WrongCat::WrongCat(const WrongCat &copy)
{
	*this = copy;
}

void		WrongCat::makeSound(void) const
{
	std::cout << "Meow." << std::endl;
}

WrongCat	&WrongCat::operator =(const WrongCat &copy)
{
	type = copy.type;
	std::cout << "WrongCat created from copy." << std::endl;

	return (*this);
}
