#include "Brain.hpp"
#include <iostream>

Brain::~Brain(void)
{
	std::cout << "Brain destroyed." << std::endl;
}

Brain::Brain(void)
{
	for (int i = 0; i < 100; i++)
		ideas[i] = "no thoughts\nhead empty";
	std::cout << "Brain created." << std::endl;
}

Brain::Brain(const Brain &copy)
{
	*this = copy;
}

std::string	Brain::get_idea(int i)
{
	return (ideas[i]);
}

Brain	&Brain::operator =(const Brain &copy)
{
	std::copy(copy.ideas, copy.ideas + 100, this->ideas);
	std::cout << "Brain created from copy." << std::endl;

	return (*this);
}

void	Brain::set_idea(int i, std::string value)
{
	ideas[i] = value;
}
