#ifndef ANIMAL_HPP
# define ANIMAL_HPP
# include "Brain.hpp"
# include <string>

class	Animal
{
	public:
		virtual			~Animal(void);
		virtual Brain	&get_brain(void) const = 0;
		std::string		getType(void) const;
		virtual void	makeSound(void) const = 0;
		Animal			&operator =(const Animal &copy);
	protected:
		Animal(void);
		Animal(const Animal &copy);
		std::string	type;
};

#endif
