#include "Animal.hpp"
#include <iostream>

Animal::~Animal(void)
{
	std::cout << "Animal of type " << type << " destroyed." << std::endl;
}

Animal::Animal(void)
{
	type = "none";
	std::cout << "Animal created." << std::endl;
}

Animal::Animal(const Animal &copy)
{
	*this = copy;
}

std::string	Animal::getType(void) const
{
	return (type);
}

Animal		&Animal::operator =(const Animal &copy)
{
	type = copy.getType();
	std::cout << "Animal of type " << type << " created from copy." << std::endl;

	return (*this);
}
