#ifndef DOG_HPP
# define DOG_HPP
# include "Animal.hpp"
# include "Brain.hpp"

class Dog: public Animal
{
	public:
		~Dog(void);
		Dog(void);
		Dog(const Dog &copy);
		Brain		&get_brain(void) const;
		void		makeSound(void) const;
		Dog			&operator =(const Dog &copy);
	private:
		Brain	*brain;
};

#endif
